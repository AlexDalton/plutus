package main

import (
	"os"
	"fmt"
	"log"
	"flag"
	"io/ioutil"
	"bufio"
	"github.com/tyler-smith/go-bip39"
    "github.com/ethereum/go-ethereum/common/hexutil"
    "github.com/ethereum/go-ethereum/crypto"
	"crypto/ecdsa"
	"regexp"
    "encoding/json"
)

type Config struct {
	BTC_addr, ETH_addr string
}

type Key struct {
	pk, sk, coin string
}

func ExpandMnemonic(phrase string) string {
	seed := bip39.NewSeed(phrase, "")

	return hexutil.Encode(seed)
}

func BuildKeys(key_info string) []Key {
	keys := []Key{}

	// convert phrase to sk
	// regex for phrase [\w\s]+
	phrase := regexp.MustCompile(`(\w\s)+\w`)

	if phrase.MatchString(key_info) {
		key_info = ExpandMnemonic(key_info)
		fmt.Println(key_info)
	}

	// if possibly BTC key generate BTC Key
	// -- pk: ^(?:[13]{1}[a-km-zA-HJ-NP-Z1-9]{26,33}|bc1[a-z0-9]{39,59})$
	btc_pk := regexp.MustCompile(`^(?:[13]{1}[a-km-zA-HJ-NP-Z1-9]{26,33}|bc1[a-z0-9]{39,59})$`)
	if btc_pk.MatchString(key_info) {
		keys = append(keys, Key { key_info, "", "BTC" })
	}
	// -- if sk convert sk to pk


	// if possibly ETH key generate ETH Key
	// to lowercase
	// -- all keys match: 0x[a-f1-9]{40}
	eth := regexp.MustCompile(`0x[a-f1-9]{40}`)
	if eth.MatchString(key_info) {
		keys = append(keys, Key { key_info, "", "ETH" })
		sk, _ := crypto.HexToECDSA(key_info)
		pk := sk.Public()
		pk_ecdsa, _ := pk.(*ecdsa.PublicKey)
		address := crypto.PubkeyToAddress(*pk_ecdsa).Hex()
		keys = append(keys, Key { address, key_info, "ETH" })
	}
	// -- if sk convert sk to pk

	return keys
}

func main() {
	var key_file string
	var config_file string

	flag.StringVar(&key_file, "k", "keys", "File containing key information, one key per line. Default: keys")
	flag.StringVar(&config_file, "c", "config", "File containing configuration information. Default: config")
	flag.Parse()

	// read config file, if one doesn't exist ask for user input to create one
	config := Config { "", "" }
	config_json, err := ioutil.ReadFile(config_file)
	if err != nil {
        fmt.Println("Unable to find config file...")
        fmt.Println("Generating config file...")

		// ask for BTC node URL
        fmt.Println("BTC node url?")
		fmt.Scanln(&config.BTC_addr)

		// ask for ETH node URL
        fmt.Println("ETH node url?")
		fmt.Scanln(&config.ETH_addr)

		// write to file
		res, _ := json.MarshalIndent(config, "", " ")
		_ = ioutil.WriteFile(config_file, res, 0644)
    } else {
		err := json.Unmarshal(config_json, &config)
		if err != nil {
			log.Fatal(err)
		}
	}
	
	fmt.Println(config)

	file, err := os.Open(key_file)

	if err != nil {
        log.Fatal(err)
    }
    defer file.Close()

	scanner := bufio.NewScanner(file)
	keys := []Key{}
	for scanner.Scan() {
		keys = append(keys, BuildKeys(scanner.Text())...)
    }
}
