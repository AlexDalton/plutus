module plutus

go 1.16

require (
	github.com/ethereum/go-ethereum v1.10.20 // indirect
	github.com/tyler-smith/go-bip39 v1.1.0 // indirect
)
